<?php

/**
 * @file
 * Rules integration for OG kill switch.
 */

/**
 * Implements hook_rules_action_info().
 */
function og_killswitch_rules_action_info() {
  $default = array(
    'parameter' => array(
      'group_node' => array('type' => 'node', 'label' => t('Group node')),
    ),
    'group' => t('Organic groups'),
  );
  $items['og_killswitch_kill'] = $default + array(
    'label' => t("Kill write access to group"),
    'callbacks' => array(
      'execute' => 'og_killswitch_rules_kill',
    ),
  );
  $items['og_killswitch_resurrect'] = $default + array(
    'label' => t("Resurrect write access to group"),
    'callbacks' => array(
      'execute' => 'og_killswitch_rules_resurrect',
    ),
  );

  return $items;
}

/**
 * Rules action callback for 'og_killswitch_kill'.
 */
function og_killswitch_rules_kill($group) {
  if (!og_killswitch_killswitch_exists($group->type)) {
    return;
  }
  $wrapper = entity_metadata_wrapper('node', $group);
  $wrapper->{OG_KILLSWITCH_FIELD}->set(TRUE);
}

/**
 * Rules action callback for 'og_killswitch_resurrect'.
 */
function og_killswitch_rules_resurrect($group) {
  if (!og_killswitch_killswitch_exists($group->type)) {
    return;
  }
  $wrapper = entity_metadata_wrapper('node', $group);
  $wrapper->{OG_KILLSWITCH_FIELD}->set(FALSE);
}
